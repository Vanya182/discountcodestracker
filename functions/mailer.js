const config = require('./config/index');
const path = require('path');
const fs = require('fs');
const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');
const mailerOptions = {
    auth: {
        api_key: config.mailing.sendGridApiKey
    }
};
const mailer = nodemailer.createTransport(sgTransport(mailerOptions));

/**
 * For constructing email templates
 */
const EmailTemplates = require('swig-email-templates');
const EmailTemplatesLoadConfig = {
    text: false, // Disable text alternatives
    swig: {
        cache: false // Don't cache swig templates
    },
    filters: {
        upper: function (str) {
            return str.toUpperCase();
        }
    },
    juice: {
        webResources: {
            images: false // Inline images under 8kB
        }
    }
};
const templates = new EmailTemplates(EmailTemplatesLoadConfig);

const sendStatisticsEmail = function (statisticsData, unusedCodesLimit) {
    return new Promise((resolve, reject) => {
        const today  = new Date();
        const dateOptionsFormat = { year: 'numeric', month: 'long', day: 'numeric' };

        let context = {
            unusedCodesLimit: unusedCodesLimit,
            statisticsData: statisticsData,
            date: today.toLocaleDateString("en-US", dateOptionsFormat)
        };

        /**
         * Loading statistics email template
         * and passing email context to it.
         */
        let userRegistrationTemplatePath = path.join(__dirname, './email-templates', 'statistics.html');
        templates.render(userRegistrationTemplatePath, context, (err, html, text, subject) => {
            let mail = {
                to: config.mailing.adminEmail,
                from: `TrebLab <${config['mailing']['senderEmail']}>`,
                subject: 'Treblab discount codes usage statistics',
                html: html,
                attachments: []
            };

            mailer.sendMail(mail, (err, response) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve();
            });
        });
    });
}

module.exports = {
    sendStatisticsEmail
};