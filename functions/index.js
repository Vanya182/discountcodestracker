const config = require('./config');
const functions = require('firebase-functions');
const mailingHelper = require('./mailer');
const firebaseAdmin = require("firebase-admin");
const generator = require('generate-password');

/**
 * If unused codes count under node is less then this -> mark this node as red in template
 */
const UNUSED_CODES_LIMIT = 100;

function sortByUnusedCodesCount(arr) {
    return arr.sort((a, b) => {
        return a.totalUnusedCodesCount - b.totalUnusedCodesCount;
    });
}

function collectStatisticsPerNode(db, databaseID, STATISTICS) {
    return new Promise((resolve, reject) => {
        /**
         * Get all documents within root node
         */
        db.ref("/").once("value", rootSnapshot => {
            const rootNode = rootSnapshot;
            const rootKey = rootSnapshot.key;
            const results = {
                id: databaseID,
                data: []
            };
            /**
             * Node stands for first level node under each country database
             */
            rootNode.forEach(nodeSnapshot => {
                const node = nodeSnapshot.val();
                const nodeKey = nodeSnapshot.key;
                const resultsForCurrentNode = {
                    id: nodeKey
                };

                let expiredCodesCount = 0;
                Object.keys(node.codes).map(code => {
                    if (node.codes[code]['expired']) {
                        expiredCodesCount += 1;
                    }
                });

                resultsForCurrentNode['totalCodesCount'] = Object.keys(node.codes).length;
                resultsForCurrentNode['expiredCodesCount'] = expiredCodesCount;
                resultsForCurrentNode['totalUnusedCodesCount'] = resultsForCurrentNode['totalCodesCount'] - resultsForCurrentNode['expiredCodesCount'];

                results.data.push(resultsForCurrentNode);
            });

            results.data = sortByUnusedCodesCount(results.data);

            STATISTICS.push(results);
            resolve();
        }, err => {
            reject(err.code);
        });
    });
}



exports.getStatisticsData = functions.https.onRequest((request, response) => {

        /**
         * Array to keep objects with statistics for each of database listed in config databases
         */
        const STATISTICS = [];
        const workersArr = [];
        /**
         * Loop through all databases available
         */
        config.databases.map(database => {
            const databaseID = database.id;
            const firebaseAdminConfiguration = require('./config/' + database.credentials);

            const connectionName = generator.generate({
                length: 15,
                numbers: true
            });
            
            const adminConnection = firebaseAdmin.initializeApp({
                credential: firebaseAdmin.credential.cert(firebaseAdminConfiguration),
                databaseURL: database.databaseURL
            }, connectionName);
            const currentDatabaseConnection = adminConnection.database();
            workersArr.push(collectStatisticsPerNode(currentDatabaseConnection, databaseID, STATISTICS));
        });

        Promise.all(workersArr).then(() => {
            mailingHelper.sendStatisticsEmail(STATISTICS, UNUSED_CODES_LIMIT).then(res => {
                response.status(200).send('Statistics were calculated. Email was sent');
            }).catch(err => {
                response.status(500).send(err);
            });
        }).catch(err => {
            response.status(500).send(err);
        });
});